# mtk-neuropilot-prebuilts

NeuroPilot is the center of MediaTek’s AI ecosystem. NeuroPilot embraces ‘Edge AI’, where AI processing is performed locally on-device.

This repo provides user space prebuilt binaries and firmwares of NeuroPilot. 



## Kernel space

In kernel space, prebuilt binaries works with the APUSYS to executing Neural Network modles with hardware acceleration.



## Branch

- main: The main branch is unused.
- kirkstone: For G1200 and G700 platforms, neuropilot version 6. It is still under development and subject to design changes.
- mt8195: For G1200 platform, neuropilot version 5. It is still under development and subject to design changes.


